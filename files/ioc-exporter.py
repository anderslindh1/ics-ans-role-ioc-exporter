"""A Python2 Prometheus exporter for IOC metadata

This script sets up a webserver that exports to Prometheus data about an IOC state.
Checks if:
- there have been local modifications made
- usage of standard utilities is correct
"""

import json
import logging
import logging.handlers
import os
import subprocess
import re
import time

import click
import git
import requests
import sdnotify
import yaml
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY, GaugeMetricFamily

IOC_CLONE_DIR = "/opt/iocs"
LOGFILE_NAME = "/var/log/prometheus/ioc-exporter.log"
MAX_LOGFILE_SIZE = 50 * 1024 * 1024  # 50MB
LOGFILE_BACKUP_COUNT = 1

ESSIOC_REGEX = re.compile(
    r"^require\s*\(?\s*(?P<q>\"?)\bessioc(?P=q)\s*\)?$", re.MULTILINE
)
ESSIOC_CONFIG_REGEX = re.compile(
    r"^iocshLoad\s*\(?\s*(?P<q>\"?)\$[\(\{]essioc_DIR[\)\}]/?(?:common|development)_config\.iocsh(?P=q)\s*\)?$",
    re.MULTILINE,
)


class IOCCollector(object):
    def __init__(self, iocdirs, logger):
        self.logger = logger
        self.iocdirs = iocdirs

    def collect(self):
        git_dirty = GaugeMetricFamily(
            "ioc_repository_dirty",
            "e3 IOC git repository has non-commited local changes",
            labels=["ioc"],
        )
        git_diff = GaugeMetricFamily(
            "ioc_repository_local_commits",
            "e3 IOC git repository has local committed changes",
            labels=["ioc"],
        )
        service_disabled = GaugeMetricFamily(
            "ioc_service_disabled",
            "e3 IOC system daemon is disabled",
            labels=["ioc"],
        )
        essioc_missing = GaugeMetricFamily(
            "ioc_essioc_missing",
            "e3 IOC does not use essioc",
            labels=["ioc"],
        )
        cellmode_active = GaugeMetricFamily(
            "ioc_cellmode_used",
            "e3 IOC uses cellmode",
            labels=["ioc"],
        )
        coredump_exists = GaugeMetricFamily(
            "ioc_coredump_exists",
            "e3 IOC has generated a coredump",
            labels=["ioc"],
        )
        for iocdir in self.iocdirs:
            deployment_path = os.path.join(os.path.abspath(iocdir), ".deployment")
            ioc = self.parse_data_file(deployment_path, "metadata")
            if ioc:
                try:
                    self.check_ioc_daemon_enabled(ioc, service_disabled)
                except Exception as error:
                    self.logger.exception(error)
                try:
                    self.check_essioc_usage(ioc, iocdir, essioc_missing)
                except Exception as error:
                    self.logger.exception(error)
                try:
                    self.check_cellmode_usage(ioc, iocdir, cellmode_active)
                except Exception as error:
                    self.logger.exception(error)
                try:
                    self.check_git_state(ioc, iocdir, git_dirty, git_diff)
                except Exception as error:
                    self.logger.exception(error)
                try:
                    self.check_coredump_exists(ioc, iocdir, coredump_exists)
                except Exception as error:
                    self.logger.exception(error)
        yield git_dirty
        yield git_diff
        yield service_disabled
        yield essioc_missing
        yield cellmode_active
        yield coredump_exists

    def check_cellmode_usage(self, ioc, iocdir, metric):
        """Exports boolean indicating whether cellmode is active."""
        cellmode_active = True  # assume the worst
        metadata = self.parse_data_file(iocdir, "ioc")
        if metadata:
            try:
                cellmode_active = bool(
                    metadata["cells"]
                )  # we only care if anything is set
            except KeyError:
                cellmode_active = False  # cellmode is inactive if cell-list is empty
        metric.add_metric([ioc["name"]], int(cellmode_active))

    def check_git_state(self, ioc, iocdir, metric_dirty, metric_diff):
        """Exports boolean showing if local/remote git repositories are out of sync."""
        try:
            repo = git.Repo(iocdir)
        except git.InvalidGitRepositoryError:
            self.logger.error(
                "Failure trying to identify git repository: {}".format(iocdir)
            )
        else:
            metric_dirty.add_metric(
                [ioc["name"]], int(repo.is_dirty(untracked_files=True))
            )
            deployed_version = repo.git.rev_parse(
                "{}^{{commit}}".format(ioc["version"])
            )
            current_version = repo.git.rev_parse(repo.commit().hexsha)
            metric_diff.add_metric(
                [ioc["name"]], int(deployed_version != current_version)
            )

    def check_ioc_daemon_enabled(self, ioc, metric):
        """Exports boolean showing if IOC service is disabled."""
        ioc_service_name = "ioc@{}.service".format(ioc["name"])
        ioc_service_unitfilestate = subprocess.check_output(
            [
                "systemctl",
                "is-enabled",
                ioc_service_name,
            ],
            universal_newlines=True,
        )
        metric.add_metric([ioc["name"]], int("disabled" in ioc_service_unitfilestate))

    def check_essioc_usage(self, ioc, iocdir, metric):
        """Exports boolean indicating if essioc is not loaded."""
        st_cmd = os.path.join(iocdir, "st.cmd")
        try:
            with open(st_cmd, "r") as f:
                contents = f.read()
        except IOError:
            contents = ""
        essioc_missing = (
            0
            if ESSIOC_REGEX.search(contents) and ESSIOC_CONFIG_REGEX.search(contents)
            else 1
        )
        metric.add_metric([ioc["name"]], essioc_missing)

    def check_coredump_exists(self, ioc, iocdir, metric):
        """Exports boolean indicating if there is a coredump file generated by the IOC process."""
        coredump_file = os.path.join(iocdir, "core")
        metric.add_metric([ioc["name"]], int(os.path.exists(coredump_file)))

    def parse_data_file(self, path, basename):
        def _parse_data_file(path, basename, extension, parse_function):
            data_file = os.path.join(path, "{}.{}".format(basename, extension))
            try:
                with open(data_file, "r") as f:
                    return parse_function(f)
            except IOError:
                self.logger.debug("Missing metadata file {}".format(data_file))
            except Exception as error:
                self.logger.error(
                    "Failure loading file {}: {}".format(data_file, str(error))
                )
            return None

        data = _parse_data_file(path, basename, "json", json.load)
        if data:
            return data
        return _parse_data_file(path, basename, "yml", yaml.safe_load)


@click.command()
@click.option("--port", default=12110, help="TCP port the exporter will listen on")
@click.option(
    "--path",
    default=IOC_CLONE_DIR,
    type=click.Path(),
    help="Root directory for IOCs",
)
def main(port, path):
    loghandler = logging.handlers.RotatingFileHandler(
        LOGFILE_NAME, maxBytes=MAX_LOGFILE_SIZE, backupCount=LOGFILE_BACKUP_COUNT
    )
    loghandler.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    logger = logging.Logger("ioc-exporter")
    logger.addHandler(loghandler)
    try:
        iocdirs = [
            os.path.join(path, f)
            for f in os.listdir(path)
            if os.path.isdir(os.path.join(path, f))
        ]
    except OSError:
        iocdirs = []
    REGISTRY.register(IOCCollector(iocdirs, logger))
    start_http_server(port)

    sdnotifier = sdnotify.SystemdNotifier()
    watchdog_sec = int(os.getenv("WATCHDOG_USEC")) / 1000000

    while True:
        # Check on the health of the exporter; if it times out or returns
        # anything but a HTTP 200, then we reboot the service.
        t0 = time.time()
        try:
            r = requests.get(
                "http://localhost:{}".format(port), timeout=watchdog_sec / 2
            )
        except:  # noqa: E722
            continue
        t1 = time.time()

        if r.status_code != 200:
            continue

        sdnotifier.notify("WATCHDOG=1")

        try:
            time.sleep(watchdog_sec / 2 - (t1 - t0))
        except IOError:  # negative sleep time
            pass


if __name__ == "__main__":
    main()
